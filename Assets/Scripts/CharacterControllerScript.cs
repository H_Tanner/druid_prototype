﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class CharacterControllerScript : MonoBehaviour
{
    [Tooltip("how high the player can jump")]
    public float JumpHeight;

    [SerializeField]
    private float Gravity = 0;

    [Tooltip("Player Camera")]
    public GameObject cam;

    [SerializeField]
    [Tooltip("How fast the player moves")]
    [Range(1, 100)]

    private CharacterController controller;
    private float PlayerMovementSpeed = 10f;

    //players direction to move
    private Vector3 MoveDirection = new Vector3(0, 0, 0);

    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        #region Movement
        //if the player is grounded allow the player to move
        if (controller.isGrounded == true)
        {
            //wasd controlls
            float x = Input.GetAxis("Horizontal");
            float z = Input.GetAxis("Vertical");
            
            //cameras rotations
            Vector3 forward = cam.transform.forward;
            Vector3 right = cam.transform.right;

            forward.y = 0;
            right.y = 0;

            MoveDirection = (forward * z + right * x) * PlayerMovementSpeed;

            //jumping
            if (Input.GetKey(KeyCode.Space))
            {
                MoveDirection.y += JumpHeight;
            }
        }
        #endregion

        //allow for gravity
        MoveDirection.y -= Gravity * Time.deltaTime;

        controller.Move(MoveDirection * Time.deltaTime);
    }
}
