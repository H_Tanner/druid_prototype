﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class DollyTransition : MonoBehaviour
{
    public CinemachineVirtualCamera Cinemachine_Virtual_Camera;
    public CinemachineSmoothPath NewTrack;

    private CinemachineTrackedDolly Cinemachine_Tracked_Dolly;

    // Start is called before the first frame update
    void Start()
    {
        Cinemachine_Tracked_Dolly = Cinemachine_Virtual_Camera.GetCinemachineComponent<CinemachineTrackedDolly>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (Cinemachine_Tracked_Dolly.m_Path != NewTrack)
            Cinemachine_Tracked_Dolly.m_Path = NewTrack;
    }
}
