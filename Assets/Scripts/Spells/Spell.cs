﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spell : MonoBehaviour
{
    public Camera cam;

    public bool IsUsingSpell = false;
    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public virtual void Interact()
    {

    }

    public virtual void spellHold(Vector3 Pos)
    {

    } 
    public virtual void PassRayPos(Vector3 Pos)
    {

    }
}
