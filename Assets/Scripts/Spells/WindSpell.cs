﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindSpell : Spell
{
    public GameObject WindPrefab;
    private GameObject Wind;
    public float RotationSpeed = 10f;

    public Quaternion Rotation = new Quaternion(0, 0, 0, 0);

    private Vector3 NewPos;

    // Update is called once per frame
    void Update()
    {
        Interact();
        if(IsUsingSpell == true)
        {
            Wind.transform.position = NewPos;
        }
    }

    public override void Interact()
    {
        if (Input.GetMouseButton(0))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider != null && IsUsingSpell == false)
                {
                    spellHold(hit.point);
                    IsUsingSpell = true;
                }
                if (IsUsingSpell == true)
                {
                    PassRayPos(hit.point);
                }
            }

            if(Input.GetKey(KeyCode.E) && IsUsingSpell == true)
            {
                Wind.transform.Rotate(new Vector3(0, RotationSpeed * Time.deltaTime, 0));
            }
            
            if(Input.GetKey(KeyCode.Q) && IsUsingSpell == true)
            {
                Wind.transform.Rotate(new Vector3(0, -RotationSpeed * Time.deltaTime, 0));
            }
        }
        if(Input.GetMouseButtonUp(0))
        {
            IsUsingSpell = false;
            Destroy(Wind);

        }
    }

    public override void spellHold(Vector3 Pos)
    {
        Wind = Instantiate(WindPrefab, Pos, Rotation);
    }
    
    public override void PassRayPos(Vector3 Pos)
    {
        NewPos = Pos;
    }
}
