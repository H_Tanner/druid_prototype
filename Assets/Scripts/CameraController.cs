﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraController : MonoBehaviour
{
    public CinemachineVirtualCamera Cinemachine_Virtual_Camera;

    [SerializeField]
    private float NewCamPos = 0;

    // Update is called once per frame
    void Update()
    {
        if (Cinemachine_Virtual_Camera.GetCinemachineComponent<CinemachineTrackedDolly>().m_PathPosition != NewCamPos)
        {
            Cinemachine_Virtual_Camera.GetCinemachineComponent<CinemachineTrackedDolly>().m_PathPosition = NewCamPos;
        }
    }

    public void SetNewCamPos(float NewPos)
    {
        NewCamPos = NewPos;
    }
}

