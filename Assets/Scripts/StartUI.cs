﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartUI : MonoBehaviour
{
    public Camera cam;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if(hit.collider != null)
                {
                    if (hit.collider.gameObject.tag == "3DButton")
                    {
                        hit.collider.gameObject.GetComponent<_3DButton>().OnClick();
                    }
                }
            }
        }
    }
}
