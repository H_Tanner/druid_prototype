﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DollyController : MonoBehaviour
{
    public float CamPos = 0;

    private GameObject Parent;
    CameraController Camera_Controller;
    
    // Start is called before the first frame update
    void Start()
    {
        Parent = transform.parent.gameObject;
        Camera_Controller = Camera.main.GetComponent<CameraController>();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("collision");
            Camera_Controller.SetNewCamPos(CamPos);
        }
    }
}
