﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class Start_3DButton : _3DButton
{

    public GameObject UI;

    public GameObject Player;

    public CameraController CamControl;

    public CinemachineVirtualCamera Cinemachine_Virtual_Camera;
    private CinemachineTrackedDolly Cinemachine_Tracked_Dolly;

    void Start()
    {
        Cinemachine_Tracked_Dolly = Cinemachine_Virtual_Camera.GetCinemachineComponent<CinemachineTrackedDolly>();
    }

    public override void OnClick()
    {
        Debug.Log("click");
        

        CamControl.SetNewCamPos(1);

        Player.GetComponent<CharacterControllerScript>().enabled = true;
        UI.SetActive(false);
    }
}
